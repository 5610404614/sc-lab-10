package controller;

import model.BankAccount;

public class Controller {
	private BankAccount bank;
	private int nonWithdraw;
	
	public Controller(){
		bank = new BankAccount(); 
	}
	
	public void deposit(String s){
		bank.deposit(s);
	}
	
	public void withdraw(String s){
		double x = Double.parseDouble(s);
		double y = bank.getBalance();
		if (y < x){
			nonWithdraw = 1;
		}
		else{
			nonWithdraw = 0;
			bank.withdraw(s);
		}
	}
	
	public double getBalance(){
		return bank.getBalance();
	}
	
	public int getValue(){
		return nonWithdraw;
	}
}
