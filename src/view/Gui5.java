package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;

public class Gui5 {
	private JFrame frame;
	private JPanel panel1;
	private JPanel panel2;
	private JMenuBar menuBar;
	private JMenu menuList;
	private JMenuItem red,green,blue;
		
	public Gui5(){
		createFrame();
	}
	
	public void createFrame(){
		frame = new JFrame();
		frame.setDefaultCloseOperation(frame.EXIT_ON_CLOSE);
		frame.setSize(500, 400);
		
		panel1 = new JPanel();
		panel1.setLayout(new BorderLayout());
		panel1.setBackground(new Color(255,255,255));
		
		panel2 = new JPanel();
		panel2.setLayout(new FlowLayout());
		panel2.setBackground(new Color(255,255,255));
		
		menuBar = new JMenuBar();
		menuList = new JMenu("Color");
		red = new JMenuItem("RED");
		green = new JMenuItem("GREEN");
		blue = new JMenuItem("BLUE");
		menuList.add(red);
		menuList.add(green);
		menuList.add(blue);	
		
		red.addActionListener(new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent e) {
				panel1.setBackground(new Color(255,0,4));	
				panel2.setBackground(new Color(255,0,4));
			}
		});
		
		green.addActionListener(new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent e) {
				panel1.setBackground(new Color(62,255,36));	
				panel2.setBackground(new Color(62,255,36));
			}
		});
		
		blue.addActionListener(new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent e) {
				panel1.setBackground(new Color(36,255,245));	
				panel2.setBackground(new Color(36,255,245));
			}
		});
		
		menuBar.add(menuList);
		panel2.add(menuBar);
		panel1.add(panel2, BorderLayout.NORTH);
		frame.setJMenuBar(menuBar);
		frame.add(panel1, BorderLayout.CENTER);	
		
		frame.setVisible(true);
	}
}
