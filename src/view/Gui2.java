package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

public class Gui2 {
	private JFrame frame;
	private JPanel panel1,panel2;
	private JRadioButton red,green,blue;
	
	public Gui2(){
		createGUI();
	}
	
	public void createGUI(){
		frame = new JFrame();
		frame.setDefaultCloseOperation(frame.EXIT_ON_CLOSE);
		frame.setSize(500, 400);
		
		panel1 = new JPanel();
		panel1.setLayout(new BorderLayout());
		panel1.setBackground(new Color(255,255,255));
		
		panel2 = new JPanel();
		panel2.setLayout(new FlowLayout());
		panel2.setBackground(new Color(255,255,255));
		
		red = new JRadioButton("RED");
		blue = new JRadioButton("BLUE");
		green = new JRadioButton("GREEN");
		
		panel2.add(red);
		panel2.add(green);
		panel2.add(blue);
		panel1.add(panel2, BorderLayout.SOUTH);
		frame.add(panel1, BorderLayout.CENTER);	
		frame.setVisible(true);
		
		red.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				panel1.setBackground(new Color(255,0,4));	
				panel2.setBackground(new Color(255,0,4));	
			}
		});

		green.addActionListener(new ActionListener() {	
			@Override
			public void actionPerformed(ActionEvent e) {
				panel1.setBackground(new Color(62,255,36));	
				panel2.setBackground(new Color(62,255,36));
			}
		});
		
		blue.addActionListener(new ActionListener() {	
			@Override
			public void actionPerformed(ActionEvent e) {
				panel1.setBackground(new Color(36,255,245));	
				panel2.setBackground(new Color(36,255,245));
			}
		});	
	}
}