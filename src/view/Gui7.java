package view;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import controller.Controller;

public class Gui7 {
	private int key = 0;
	private Controller control;
	private JFrame frame;
	private JPanel panelMain,panel,panel2;
	private JMenuBar menuBar;
	private JMenu option;
	private JMenuItem deposit,withdraw;
	private JLabel title;
	private JTextField money;
	private JButton submit;
	private JTextArea balance;
		
	public Gui7(){
		control = new Controller();
		createFrame();
	}
	
	public void createFrame(){
		frame = new JFrame();
		frame.setDefaultCloseOperation(frame.EXIT_ON_CLOSE);
		frame.setSize(600, 300);
		
		panelMain = new JPanel();
		panelMain.setLayout(new FlowLayout());
		
		panel = new JPanel();
		panel.setLayout(new FlowLayout());
		
		panel2 = new JPanel();
		panel2.setLayout(new FlowLayout());
		
		
		menuBar = new JMenuBar();
		
		option = new JMenu("Option");
		deposit = new JMenuItem("Deposit");
		withdraw = new JMenuItem("Withdraw");
	
		option.add(deposit);
		option.add(withdraw);
		
		deposit.addActionListener(new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent e) {
				key = 0;
				title.setText(" �ҡ�ӹǹ�Թ : ");
			}
		});
		
		withdraw.addActionListener(new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent e) {
				key = 1;
				title.setText(" �͹�ӹǹ�Թ : ");
			}
		});
		
		title = new JLabel(" �ҡ�ӹǹ�Թ : ");		
		
		money = new JTextField(9);
		
		submit = new JButton("��ŧ");
		
		submit.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if (key == 0){
					control.deposit(money.getText());
					balance.append("�ӹǹ�Թ�ҡ :  " + money.getText() + "    �ҷ\n");
					balance.append("�ʹ������� :  " + control.getBalance() + "    �ҷ\n");

				}
				else{
					control.withdraw(money.getText());
					if (control.getValue() == 1){
						balance.append("�������ö�͹�Թ��\n");
					}
					else{						
						balance.append("�ӹǹ�Թ�͹ :  " + money.getText() + "    �ҷ\n");
						balance.append("�ʹ������� :  " + control.getBalance() + "    �ҷ\n");
					}
				}
			}
		});
		
		
		balance = new JTextArea(8,15);
		
		menuBar.add(option);
		panel.add(menuBar);
		panel.add(title);
		panel.add(money);
		panel.add(submit);
		panel2.add(balance);
		panelMain.add(panel);
		frame.add(panelMain);
		frame.add(panel2, BorderLayout.SOUTH);
		frame.setJMenuBar(menuBar);
		frame.setVisible(true);
	}
}