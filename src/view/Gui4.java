package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class Gui4 {
	private JFrame frame;
	private JPanel panel1,panel2;
	private JComboBox<String> listCheckBox;
	
	public Gui4(){
		createGUI();
	}
	
	public void createGUI(){
		frame = new JFrame();
		frame.setDefaultCloseOperation(frame.EXIT_ON_CLOSE);
		frame.setSize(500, 400);
		
		panel1 = new JPanel();
		panel1.setLayout(new BorderLayout());
		panel1.setBackground(new Color(255,255,255));
		
		panel2 = new JPanel();
		panel2.setLayout(new FlowLayout());
		panel2.setBackground(new Color(255,255,255));
		
		listCheckBox = new JComboBox<String>();
		listCheckBox.addItem("RED");
		listCheckBox.addItem("GREEN");
		listCheckBox.addItem("BLUE");	
		
		listCheckBox.addActionListener(new ListenerMgr());
		
		panel2.add(listCheckBox);
		panel1.add(panel2, BorderLayout.SOUTH);
		frame.add(panel1, BorderLayout.CENTER);		
		frame.setVisible(true);
	}
	
	class ListenerMgr implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
			if (listCheckBox.getSelectedItem().equals("Red")){
				panel1.setBackground(new Color(255,0,4));	
				panel2.setBackground(new Color(255,0,4));
			}
			else if (listCheckBox.getSelectedItem().equals("Green")){
				panel1.setBackground(new Color(62,255,36));	
				panel2.setBackground(new Color(62,255,36));
			}
			else if (listCheckBox.getSelectedItem().equals("Blue")){
				panel1.setBackground(new Color(36,255,245));	
				panel2.setBackground(new Color(36,255,245));
			}
		}
	}
}
