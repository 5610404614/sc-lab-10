package view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JTextField;

import controller.Controller;

public class Gui6 {
		private int key;
		private Controller control;
		private JFrame frame;
		private JButton submit;
		private JMenuBar menuBar;
		private JMenu option;
		private JMenuItem deposit,withdraw;
		private JLabel title,amount,balance;
		private JTextField money;
		

		public Gui6(){
			control = new Controller();
			createFrame();
		}
		
		public void createFrame(){
			frame = new JFrame();
			frame.setLayout(null);
			frame.setDefaultCloseOperation(frame.EXIT_ON_CLOSE);
			frame.setSize(300, 400);
			
			menuBar = new JMenuBar();
			
			option = new JMenu("Option");
			deposit = new JMenuItem("Deposit");
			withdraw = new JMenuItem("Withdraw");
		
			option.add(deposit);
			option.add(withdraw);
			
			deposit.addActionListener(new ActionListener() {			
				@Override
				public void actionPerformed(ActionEvent e) {
					key = 0;
					title.setText("�ҡ�Թ");
					amount.setText("��͡�ӹǹ�Թ�ҡ");	
				}
			});
			
			withdraw.addActionListener(new ActionListener() {			
				@Override
				public void actionPerformed(ActionEvent e) {
					key = 1;
					title.setText("�͹�Թ");
					amount.setText("��͡�ӹǹ�Թ�͹");
				}
			});
			
			title = new JLabel("�ҡ�Թ");
			title.setBounds(121, 20, 100, 50);
			
			amount = new JLabel("��͡�ӹǹ�Թ�ҡ");
			amount.setBounds(35, 60, 350, 50);

			balance = new JLabel("�ʹ������� :  0.0    �ҷ");
			balance.setBounds(65, 185, 180, 50);
			
			money = new JTextField();
			money.setBounds(85, 115, 100, 20);

			menuBar.add(option);
			frame.add(menuBar);
			frame.add(title);
			frame.add(amount);
			frame.add(money);
			frame.add(submit);
			frame.add(balance);
			frame.setJMenuBar(menuBar);
			frame.setVisible(true);
			
			submit = new JButton("��ŧ");
			submit.setBounds(95, 157, 80, 20);
			
			submit.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					String a = amount.getText();
					if (key == 0){
						control.deposit(a);
						balance.setText("�ʹ������� :  " + control.getBalance());
					}
					else{
						control.withdraw(a);
						balance.setText("�ʹ������� :  " + control.getBalance());
					}
				}
			});
		}
}
