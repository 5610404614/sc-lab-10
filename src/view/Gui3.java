package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class Gui3 {
	private JFrame frame;
	private JPanel panel1, panel2;
	private JCheckBox red, green, blue;

	public Gui3() {
		createGUI();
	}

	public void createGUI() {
		frame = new JFrame();
		frame.setDefaultCloseOperation(frame.EXIT_ON_CLOSE);
		frame.setSize(500, 400);

		panel1 = new JPanel();
		panel1.setLayout(new BorderLayout());
		panel1.setBackground(new Color(255, 255, 255));

		panel2 = new JPanel();
		panel2.setLayout(new FlowLayout());
		panel2.setBackground(new Color(255, 255, 255));

		red = new JCheckBox("RED");
		red.addActionListener(new ListenerMgr());

		green = new JCheckBox("GREEN");
		green.addActionListener(new ListenerMgr());

		blue = new JCheckBox("BLUE");
		blue.addActionListener(new ListenerMgr());

		panel2.add(red);
		panel2.add(green);
		panel2.add(blue);

		panel1.add(panel2, BorderLayout.SOUTH);
		frame.add(panel1, BorderLayout.CENTER);
		frame.setVisible(true);
	}

	class ListenerMgr implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			if (red.isSelected() && green.isSelected() && blue.isSelected()) {
				panel1.setBackground(new Color(0, 0, 0));
				panel2.setBackground(new Color(0, 0, 0));
			
			} else if (red.isSelected()) {
				panel1.setBackground(new Color(255, 0, 4));
				panel2.setBackground(new Color(255, 0, 4));
				if (green.isSelected()) {
					panel1.setBackground(new Color(240, 247, 15));
					panel2.setBackground(new Color(240, 247, 15));
					if (blue.isSelected()) {
						panel1.setBackground(new Color(62, 15, 247));
						panel2.setBackground(new Color(62, 15, 247));
					}
				} else if (blue.isSelected()) {
					panel1.setBackground(new Color(224, 15, 247));
					panel2.setBackground(new Color(224, 15, 247));
					if (green.isSelected()) {
						panel1.setBackground(new Color(62, 15, 247));
						panel2.setBackground(new Color(62, 15, 247));
					}
				}
			
			} else if (green.isSelected()) {
				panel1.setBackground(new Color(62, 255, 36));
				panel2.setBackground(new Color(62, 255, 36));
				if (blue.isSelected()) {
					panel1.setBackground(new Color(15, 247, 174));
					panel2.setBackground(new Color(15, 247, 174));
					if (red.isSelected()) {
						panel1.setBackground(new Color(62, 15, 247));
						panel2.setBackground(new Color(62, 15, 247));
					}
				} else if (red.isSelected()) {
					panel1.setBackground(new Color(240, 247, 15));
					panel2.setBackground(new Color(240, 247, 15));
					if (blue.isSelected()) {
						panel1.setBackground(new Color(62, 15, 247));
						panel2.setBackground(new Color(62, 15, 247));
					}
				}
			
			} else if (blue.isSelected()) {
				panel1.setBackground(new Color(36, 255, 245));
				panel2.setBackground(new Color(36, 255, 245));
				if (green.isSelected()) {
					panel1.setBackground(new Color(15, 247, 174));
					panel2.setBackground(new Color(15, 247, 174));
					if (red.isSelected()) {
						panel1.setBackground(new Color(62, 15, 247));
						panel2.setBackground(new Color(62, 15, 247));
					}
				} else if (red.isSelected()) {
					panel1.setBackground(new Color(224, 15, 247));
					panel2.setBackground(new Color(224, 15, 247));
					if (green.isSelected()) {
						panel1.setBackground(new Color(62, 15, 247));
						panel2.setBackground(new Color(62, 15, 247));
					}
				}
			}
		}
	}
}