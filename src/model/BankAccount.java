package model;

public class BankAccount {
	private double balance;
	
	public void deposit(String amount){
		double a = Double.parseDouble(amount);
		balance = balance + a;
	}
	
	public void withdraw(String amount){
		double a = Double.parseDouble(amount);
		balance = balance - a;
	}
	
	public double getBalance(){
		return balance ;
	}
}